db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);
//1.
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: {_id: "$onSale", fruitsOnSale: {$count:{}}}
  },
  {
    $project: {_id:0}
  }
]);

//2.
db.fruits.aggregate([
  {$match: {$and: [{stock: {$gte : 20}}, {onSale: true}]}},
  {$count : "enoughStock" }
]);

//3.
db.fruits.aggregate([
  {$match: {onSale: true}},
  {$group: 
    {
      _id: "$supplier_id", avgPrice: {$avg: "$price"}
    }
  },
  {$sort: {avgPrice: -1} }
])

//4.
db.fruits.aggregate([
  {$group: {_id: "$supplier_id", maxPrice: {$max: "$price"}}},
  {$sort: {maxPrice: -1}}

]);

//5.
db.fruits.aggregate([
  {$group:{_id:"$supplier_id", minPrice: {$min: "$price"}}},
  {$sort: {minPrice:1}}
]);